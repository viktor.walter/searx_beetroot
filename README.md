# Searx_beetroot

Disroot theme for Searx.

To use this theme, `git clone` it on your computer.

Create a `beetroot` folder in `/var/www/searx/searx/static/themes` and in `/var/www/searx/searx/templates`.

Then, copy the content of `beetroot_static` in `/var/www/searx/searx/static/themes/beetroot` and the content of `beetroot_template` in `/var/www/searx/searx/templates/beetroot`.

Enable the beetroot theme by editing `/var/www/searx/searx/settings.yml` and changing:
```
default_theme : beetroot
theme_args :
    beetroot_style : logicodev
```

Restart searx: `service uwsgi restart`


## NOTE:
The minify versions of the css files are missing at the moment. So, to test things as they are now, edit `/var/www/searx/searx/templates/beetroot/base.html` and replace `<link rel="stylesheet" href="{{ url_for('static', filename='css/'+preferences.get_value('beetroot-style')+'.min.css') }}" type="text/css" />` by `<link rel="stylesheet" href="{{ url_for('static', filename='css/'+preferences.get_value('beetroot-style')+'.css') }}" type="text/css" />`
